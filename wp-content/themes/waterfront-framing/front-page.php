<?php
/**
* Front page template
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package waterfront-framing
 */

get_header(); ?>

<?php get_template_part( 'template-parts/hero'); 	?>
<?php get_template_part( 'template-parts/hero-gallery'); 	?>

<div id="content" class="site-content">
<section id="primary">
	<div id="main" class="site-main" role="main">
		<?php
			while ( have_posts() ) : the_post(); ?>
			<div class="container">
				<div class="row">
					<div class="col-md-6 about-content pad-100"  data-aos="fade" data-aos-duration="1000">
					<?php the_field('about_content');?>
					</div>
				</div>
			</div>
			<?php get_template_part( 'template-parts/home-contact'); 	?>
			<?php get_template_part( 'template-parts/home-do'); 	?>
		<?php endwhile; // End of the loop. ?>
	</div><!-- #main -->
</section><!-- #primary -->
<?php
get_footer();
