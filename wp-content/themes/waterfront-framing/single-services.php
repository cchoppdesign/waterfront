<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>
<?php get_template_part('template-parts/internal-banner'); ?>

		<div id="main" class="site-main" role="main">
		<div class="container service-container">
		<div class="row">

		<?php
		while ( have_posts() ) : the_post(); ?>
		<div class="col-md-9">
	 	<?php	get_template_part( 'template-parts/flex'); ?>
		</div>
		<div class="col-md-3">
			<?php the_field('service_contact_form'); ?>
		</div>
		<?php endwhile; // End of the loop.
		?>
		</div>
		</div>
		</div><!-- #main -->


<?php
get_footer();
