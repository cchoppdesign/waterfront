<?php
/**
 * Default Events Template
 * This file is the basic wrapper template for all the views if 'Default Events Template'
 * is selected in Events -> Settings -> Display -> Events Template.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/default-template.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.23
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

get_header();
?>
<section id="tertiary-banner" class="events-banner">
	<div class="container">
		<div class="row">
			<div class="col-md-12 tert-content" data-aos="fade" data-aos-duration="1000">
				<?php the_title( '<h1>', '</h1>' ); ?>
				<?php echo tribe_events_event_schedule_details( $event_id, '<h4>', '</h4>' ); ?>
			</div>
			<span id="event-graphic"></span>
		</div>
	</div>
</section>
<main id="tribe-events-pg-template" class="tribe-events-pg-template">
	<?php tribe_events_before_html(); ?>
	<?php tribe_get_view(); ?>
	<?php tribe_events_after_html(); ?>
</main> <!-- #tribe-events-pg-template -->
<?php
get_footer();
