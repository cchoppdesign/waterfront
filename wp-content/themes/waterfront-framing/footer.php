<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package waterfront-framing
 */

?>

</div><!-- #content -->

	<footer role="contentinfo">
    <section class="container">
      <div class="row">
        <div class="col-md-2 footer-logo" data-aos="fade">
          <?php dynamic_sidebar( 'footer-1' ); ?>
        </div>
        <div class="col-md-3 social" data-aos="fade" data-aos-delay="100">
					<?php dynamic_sidebar( 'footer-2' ); ?>
					<ul id="social-list">
 	 		    <?php if(get_field('social', 'options')['facebook']) { ?>
 	 		    <li><a href="<?php echo get_field('social', 'options')['facebook']; ?>"><i class="fab fa-facebook-f"></i></a></li>
 	 		    <?php }
 	 		    if(get_field('social', 'options')['instagram']) { ?>
 	 		    <li><a href="<?php echo get_field('social', 'options')['instagram']; ?>"><i class="fab fa-instagram"></i></a></li>
 	 		  <?php }
 	 		  if(get_field('social', 'options')['email']) { ?>
 	 		  <li><a href="mailto:<?php echo get_field('social', 'options')['email']; ?>"><i class="far fa-envelope"></i></a></li>

 	 		    <?php } ?>
 	 		  </ul>
						<p class="cr">
							Copywrite&copy; <?php echo date("Y"); ?> Waterfront Framing &amp; Design
						</p>

        </div>
        <div class="col-md-3" data-aos="fade" data-aos-delay="300">
          <?php dynamic_sidebar( 'footer-3' ); ?>
        </div>
        <div class="col-md-4 footer-nav" data-aos="fade" data-aos-delay="450">
					<?php dynamic_sidebar( 'footer-4' ); ?>
        </div>
      </div>
    </section>



	</footer><!-- #colophon -->
</div><!-- #page -->


<script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
<?php wp_footer(); ?>

<script>
  AOS.init({
		easing: 'ease',
		duration: 900,
		once: true
	});

	$('.hero-carousel').slick({

	  centerMode: false,
		slidesToShow: 3,
		variableWidth: true,
		prevArrow:"<button type='button' class='slick-prev'><i class='fa fa-angle-left' aria-hidden='true'></i></button>",
	  nextArrow:"<button type='button' class='slick-next'><i class='fa fa-angle-right' aria-hidden='true'></i></button>",

	});



</script>

</body>
</html>
