<?php
/**
 * Template Name: Contact Template
 *
 *
 * @package waterfront-framing
 */

get_header(); ?>
<?php get_template_part('template-parts/internal-banner'); ?>
<section id="primary">
	<div id="main" class="container site-main" role="main">
		<div class="row">
			<div class="col-lg-8">
				<?php
            while (have_posts()) : the_post();
            the_content(); ?>

				<?php
            endwhile; // End of the loop.
            ?>

			</div>
			<?php if ( is_page( array('contact-us') ) ) {
    			dynamic_sidebar('contact-sidebar');
				} else {
				     // none of the page about us, contact or management is in view
				}
				?>
		</div>
	</div><!-- #main -->
</section><!-- #primary -->

<?php
get_footer();
