<?php
/**
 * Template Name: Our Services
 *
 *
 * @package waterfront-framing
 */

get_header(); ?>
<?php get_template_part('template-parts/internal-banner'); ?>
<section id="primary">
	<div id="main" class="site-main" role="main">
		<div class="container service-listing">


		<div class="row">

		<?php
		while ( have_posts() ) : the_post(); ?>

			<?php $args = array(
				'post_type' => 'services',
				'posts_per_page' => -1
				);
				$loop = new WP_Query( $args );
				while ( $loop->have_posts() ) : $loop->the_post();
				?>
					<?php	get_template_part( 'template-parts/service'); ?>
					<?php endwhile; ?>
		<?php endwhile; // End of the loop.
		?>
		</div>

	</div>
	<div class="container" data-aos="fade">
		<div class="row ">
			<div class="col-sm-7 mx-auto prebuilt-social">
				<h3>Follow Us Online</h3>
				<ul id="social-list">
				<?php if(get_field('social', 'options')['facebook']) { ?>
				<li><a href="<?php echo get_field('social', 'options')['facebook']; ?>"><i class="fab fa-facebook-f"></i></a></li>
				<?php }
				if(get_field('social', 'options')['instagram']) { ?>
				<li><a href="<?php echo get_field('social', 'options')['instagram']; ?>"><i class="fab fa-instagram"></i></a></li>
			<?php }
			if(get_field('social', 'options')['email']) { ?>
			<li><a href="mailto:<?php echo get_field('social', 'options')['email']; ?>"><i class="far fa-envelope"></i></a></li>

				<?php } ?>
			</ul>
			</div>
		</div>
	</div>

	</div><!-- #main -->
</section><!-- #primary -->
<?php
get_footer();
