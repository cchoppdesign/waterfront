<?php
	 add_action( 'wp_enqueue_scripts', 'waterfront_framing_enqueue_styles' );
	 function waterfront_framing_enqueue_styles() {
 		  wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
			wp_enqueue_style( 'child-style', get_stylesheet_uri(), array( 'parent-style' ) );
			wp_enqueue_style( 'slick', get_stylesheet_directory_uri() . '/css/slick.css', array( 'child-style' ) );
			wp_enqueue_style( 'aos', get_stylesheet_directory_uri() . '/css/aos.css', array( 'child-style' ) );
 		  }

			/*add my custom jquery script*/
			add_action( 'wp_enqueue_scripts', 'dryboat_menu_scripts' );

			function dryboat_menu_scripts() {

			wp_enqueue_script('aos', get_stylesheet_directory_uri() . '/js/aos.js', array( 'jquery' ), false, true);
			wp_enqueue_script('slick', get_stylesheet_directory_uri() . '/js/slick.min.js', array( 'jquery' ), false, true);

			}

			function cc_mime_types($mimes) {
			  $mimes['svg'] = 'image/svg+xml';
			  return $mimes;
			}
			add_filter('upload_mimes', 'cc_mime_types');



			function waterfront_widgets_init() {
			    register_sidebar( array(
			        'name'          => esc_html__( 'Footer 4', 'waterfront-framing' ),
			        'id'            => 'footer-4',
			        'description'   => esc_html__( 'Add widgets here.', 'waterfront-framing' ),
			        'before_widget' => '<section id="%1$s" class="widget %2$s">',
			        'after_widget'  => '</section>',
			        'before_title'  => '<h3 class="widget-title">',
			        'after_title'   => '</h3>',
			    ) );

					register_sidebar( array(
			        'name'          => esc_html__( 'Contact Sidebar', 'waterfront-framing' ),
			        'id'            => 'contact-sidebar',
			        'description'   => esc_html__( 'Add widgets here.', 'waterfront-framing' ),
			        'before_widget' => '<section id="%1$s" class="widget col-lg-4 %2$s">',
			        'after_widget'  => '</section>',
			        'before_title'  => '<h3 class="widget-title">',
			        'after_title'   => '</h3>',
			    ) );



			}
			add_action( 'widgets_init', 'waterfront_widgets_init' );

			if( function_exists('acf_add_options_page') ) {
				acf_add_options_page();
			}

			/**
		 * Generate breadcrumbs
		 */
		function get_breadcrumb() {
		    echo '<a href="'.home_url().'" rel="nofollow">Home</a>';
		    if (is_category() || is_single()) {
		        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;";
		        the_category(' &bull; ');
		            if (is_single()) {
		                echo " &nbsp;&nbsp;&#187;&nbsp;&nbsp; ";
		                the_title();
		            }
		    } elseif (is_page()) {
		        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;";
		        echo the_title();
		    } elseif (is_search()) {
		        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;Search Results for... ";
		        echo '"<em>';
		        echo the_search_query();
		        echo '</em>"';
		    }
		}

 ?>
