<div class="post-block col-lg-4 col-md-12" data-aos="fade-up">
	<a href="<?php echo get_permalink(); ?>">
		<div class="pod-wrapper">
			<div class="thumb-wrapper">
				<div class="pod-explore">
					<p>Learn more</p>
				</div>
				<span></span>
				<?php if(has_post_thumbnail()) : ?>
				<?php the_post_thumbnail(); ?>
				<?php endif; ?>
      	<h4><?php the_title(); ?></h4>
			</div>
			<div>

			</div>
		</div>
	</a>
</div>
