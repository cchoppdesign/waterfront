<section class="flex">
  <?php
  // Check value exists.
  if( have_rows('content_sections') ):
      // Loop through rows.
      while ( have_rows('content_sections') ) : the_row();
          // Case: Paragraph layout.
          if( get_row_layout() == 'section_intro' ):
            $cs_title = get_sub_field('section_title');
            $cs_content = get_sub_field('section_content');
            $cs_cta = get_sub_field('section_cta');
            $cta_label = get_sub_field('section_cta_button_label');

            ?>
              <div class="container">
                <div class="row section-intro pad-100">
                  <div class="col-md-3 mx-auto">
                    <h2><?php echo $cs_title;?></h2>
                  </div>
                  <div class="col-md-7 mx-auto">
                    <?php echo $cs_content;?>
                  <a class="btn primary" href="<?php echo $cs_cta;?>"><?php echo $cta_label;?></a>
                  </div>
                </div>
              </div>
          <?php // Case: Content Left, Image Right Dark BG Call Out
          elseif( get_row_layout() == 'cr_il_dk' ):
              $cr = get_sub_field('copy_right');
              $il = get_sub_field('img_left'); ?>
              <div class="call-out-half">
                <div class="container">
                  <div class="row pad-100">
                    <div class="col-md-6">
                      <?php echo $cr; ?>
                    </div>
                    <div class="col-md-6 il-wrapper">
                      <div>
                          <img src="<?php echo $il; ?>">
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <?php // Case: Dual Content Col
              elseif( get_row_layout() == 'two_col_content' ):
                  $dual_content = get_sub_field('dual_content');
                  ?>
                  <div class="dual-content">
                    <div class="container">
                      <div class="row pad-100">
                        <div class="col-md-12 dc-content">
                          <?php echo $dual_content ; ?>
                        </div>
                      </div>
                    </div>
                  </div>


            <?php // Case: General Content
                  elseif( get_row_layout() == 'gen_content' ):
                    $gen_cont_block = get_sub_field('gen_content_block');
                      ?>

                        <div class="container">
                          <div class="row">
                            <div class="col-md-12">
                              <?php echo $gen_cont_block; ?>
                            </div>
                          </div>
                        </div>



            <?php // Load a prebuilt comp from the components PHP list
                  elseif( get_row_layout() == 'pb_comp' ):

                 if( get_sub_field( 'component' ) == "social" ) {
                   get_template_part( 'template-parts/social');
                }
                  // END THE PREBUILT CONTENT ?>

          <?php  //END cases
          endif;


      endwhile; //end the loop
  else :
  endif;
  ?>
</section>
