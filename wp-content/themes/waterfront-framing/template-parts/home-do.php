<section class="pad-100 service-options" data-aos="fade" data-aos-duration="1000">
	<div class="container">
		<div class="row">
			<div class="col">
				<h2>What We Do</h2>
			</div>
		</div>
		<div class="row">
			<?php if( have_rows('sevice_options') ): ?>
	    <?php while( have_rows('sevice_options') ): the_row();
	        $scont = get_sub_field('service_content');
					$slink = get_sub_field('service_link');
	        ?>
	        <div class="col-lg-3 col-md-6 col-sm-12 service-option">
	            <?php echo $scont; ?>
							<a class="btn primary" href="<?php echo $slink; ?>">Explore</a>
	        </div>
	    <?php endwhile; ?>
			<?php endif; ?>
		</div>
	</div>
</section>
