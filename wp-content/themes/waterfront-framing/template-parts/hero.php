<section id="hero-banner">
	<div class="container">
		<div class="row">
			<div class="col-lg-7 col-md-9" id="banner-content">
				<div data-aos="fade" data-aos-duration="2000">
					<?php the_field('hero_content'); ?>
				</div>
				<?php if( have_rows('cta_links') ): ?>
				<ul class="ctas">
					<?php while( have_rows('cta_links') ): the_row();
			        $ctas = get_sub_field('cta_item');
							$cta_label= get_sub_field('cta_label');
			        ?>
					<li data-aos="fade" data-aos-delay="800">
						<a class="btn primary" href="<?php echo esc_url( $ctas ); ?>"><?php echo $cta_label ?></a>
					</li>
					<?php endwhile; ?>
				</ul>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
