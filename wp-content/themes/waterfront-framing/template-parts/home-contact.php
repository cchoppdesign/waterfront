<section class="home-contact" data-aos="fade" data-aos-duration="2000">
	<div class="tan pad-100 bottom-0">
		<div class="container">
			<div class="row ">
				<div class="col-md-5">
					<?php the_field('hc_general_contact'); ?>
					<?php if( have_rows('operation_hours') ): ?>
					<ul class="hours-operation">
						<?php while( have_rows('operation_hours') ): the_row();
			        $oday = get_sub_field('o_day');
						 	$ohours = get_sub_field('o_hours');
			        ?>
						<li>
							<div class="oday">
								<?php echo $oday; ?>
							</div>
							<div class="ohours">
								<?php echo $ohours; ?>
							</div>
						</li>
						<?php endwhile; ?>
					</ul>
					<?php endif; ?>
					<div id="contact-options">
						<a href="tel:2699823470"><i class="fas fa-phone"></i>(269) 982-3470</a>
						<a href="mailto:valerie.glorygirl@yahoo.com"><i class="far fa-envelope"></i>Email us</a>
					</div>
				</div>
				<div class="col-md-5 support-img ml-auto drop-bottom">
					<div>
						<img src="<?php the_field('supporting_image')?>">
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
