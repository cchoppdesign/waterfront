<?php
	if(have_posts()) :
	while(have_posts()) : the_post();
		// Check value exists.
	if( have_rows('banner_styles') ):

    // Loop through rows.
    while ( have_rows('banner_styles') ) : the_row();


        if( get_row_layout() == 'child_banner' ):
            $child_text = get_sub_field('child_banner_text');
						$child_img = get_sub_field('child_image');
						?>
<section id="secondary-banner">
	<div class="container">
		<div class="row" data-aos="fade" data-aos-duration="1000">
			<div class="col-lg-5 sec-content">
				<?php echo $child_text; ?>
			</div>

			<div class="col-lg-7 support-img ml-auto drop-bottom">
				<div id="child-img-wrapper">
					<img src="<?php echo $child_img; ?>">
				</div>
			</div>
		</div>
</section>

<?php
        elseif( get_row_layout() == 'tertiary_banner' ):
            $tert_text = get_sub_field('tertiary_banner_text');

						?>
<section id="tertiary-banner">
	<div class="container">
		<div class="row">
			<div class="col-md-12 tert-content" data-aos="fade" data-aos-duration="1000">
				<?php echo $tert_text; ?>
			</div>
		</div>
	</div>
</section>

<?php  endif;

    // End loop.
    endwhile;

// No value.
else :
    // Do something...
endif; ?>

<?php endwhile; ?>
<?php endif; ?>
