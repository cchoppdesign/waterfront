<section id="hero-gallery" data-aos="fade" data-aos-duration="2000">
	<?php if( have_rows('carousel') ): ?>
	<div class="hero-carousel">
		<?php while( have_rows('carousel') ): the_row();
	        $image = get_sub_field('carousel_image');
					$ctitle = get_sub_field('carousel_title');
					$clink = get_sub_field('carousel_link');
	        ?>
		<div class="wf-slide" style="background:url('<?php echo $image ?>')no-repeat;">
			<a href="<?php echo $clink; ?>">
				<div class="view-icon">
					<div class="view-icon-wrapper">
						<p>
							View
						</p>
						<img src="/wp-content/themes/waterfront-framing/img/icon-view.svg" />
					</div>
				</div>
			<h4><?php echo $ctitle?></h4>
			</a>
		</div>
		<?php endwhile; ?>
	</div>
	<?php endif; ?>
</section>
