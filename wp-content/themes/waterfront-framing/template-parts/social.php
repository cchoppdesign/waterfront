<div class="container" data-aos="fade">
	<div class="row ">
		<div class="col-sm-7 mx-auto prebuilt-social">
			<h3>Follow Us Online</h3>
			<ul id="social-list">
			<?php if(get_field('social', 'options')['facebook']) { ?>
			<li><a href="<?php echo get_field('social', 'options')['facebook']; ?>"><i class="fab fa-facebook-f"></i></a></li>
			<?php }
			if(get_field('social', 'options')['instagram']) { ?>
			<li><a href="<?php echo get_field('social', 'options')['instagram']; ?>"><i class="fab fa-instagram"></i></a></li>
		<?php }
		if(get_field('social', 'options')['email']) { ?>
		<li><a href="mailto:<?php echo get_field('social', 'options')['email']; ?>"><i class="far fa-envelope"></i></a></li>

			<?php } ?>
		</ul>
		</div>
	</div>
</div>
