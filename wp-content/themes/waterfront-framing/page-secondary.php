<?php
/**
 * Template Name: Landing Page Template
 *
 *
 * @package waterfront-framing
 */

get_header(); ?>
<?php get_template_part('template-parts/internal-banner'); ?>
<section id="primary">
	<div id="main" class="site-main" role="main">
				<?php
            while (have_posts()) : the_post();  ?>
						<?php get_template_part( 'template-parts/flex'); 	?>
				<?php  endwhile; // End of the loop. ?>
	</div><!-- #main -->
</section><!-- #primary -->
<?php
get_footer();
