<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '0rmcSW0A+vxJwR1FS7ZtPp07saNvxjvz0JM3pGQrgyYQ/eHVq+ZjCK9mYvQICwS8HTTmr14Xg9rzaH14+1rh7w==');
define('SECURE_AUTH_KEY',  'tTCJKzDoGI7FyFQXa+54GSbg7dhmQ+cILtJdNy0PHUKhQCugkNiVBijjY/cxPOFFWtNElnbKkKEmtChlM7a+ZA==');
define('LOGGED_IN_KEY',    'JUr4xuoeK7hEUrGlynLQg1o2B7OB8kkn5m8HcpWuaErsOYkb818K/tCjGf7WIcBanpCBKeAyP73wQr/M/GQZJQ==');
define('NONCE_KEY',        'ND1vj1sE+TsSqmmOr9uXqIMluB4E8vUtQwPT9k//oy60ZG4iAk7PpwR34NBDcxHVMzkJLrQd6NFZvtJQ/cy6QQ==');
define('AUTH_SALT',        'KhxbcL8G6z1CIjyuuAdAiA39akE+i0JXPdy57u4+bfCGyR5SNX77QzWHpyJcARm1exknNNEZEKQGuWgX2u1wlg==');
define('SECURE_AUTH_SALT', 'rmKtRXj++vXi4H4dQ1lzmBq+5T/GMxpIvgwif7otW9uCEke1KBahN5BRcO9wXAOWag1rtIluHI/A3tN7RA66xA==');
define('LOGGED_IN_SALT',   'meS5rBLvUpurNeK6yjghvtVT43zwp6/pMHgv1ILPQB83fQNJFEhdQbAg+dcl0farczLsorJi0kWjattUa+wbYw==');
define('NONCE_SALT',       '5BQNi+uNjT2V/HpfP+bj+dq6ed4lCxhz7/BsgiIMEg0xE2MXmCzTzkkWSmy0TWqvxYrzd/jDCsOhWBApIafm/Q==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
